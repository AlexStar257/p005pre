
var nombre = document.getElementById('idnombre').value;
var usuario = document.getElementById('idusuario').value;
var email = document.getElementById('idemail').value;
var calle = document.getElementById('idcalle').value;
var numero = document.getElementById('idnumero').value;
var ciudad = document.getElementById('idciudad').value;

function cargarAjax() {
    var identificador = document.getElementById('idbuscar').value;
    const url = "https://jsonplaceholder.typicode.com/users"
    axios
        .get(url)
        .then((res) => {
            mostrar(res.data)
        })
        .catch((error) => {
            console.log("Ha ocurrido un error inesperado :C");
        })
    function mostrar(data) {
        for (let item of data) {
            console.log(item.id)
            console.log(identificador)
            if (item.id.toString() == identificador) {
                document.getElementById('idnombre').value = item.name;
                document.getElementById('idusuario').value = item.username;
                document.getElementById('idemail').value = item.email;
                document.getElementById('idciudad').value = item.address.city;
                document.getElementById('idnumero').value = item.address.suite;
                document.getElementById('idcalle').value = item.address.street;
                return
            }
        }
        document.getElementById('idnombre').value = "";
        document.getElementById('idusuario').value = "";
        document.getElementById('idemail').value = "";
        document.getElementById('idciudad').value = "";
        document.getElementById('idnumero').value = "";
        document.getElementById('idcalle').value = "";
        alert("No se encontró el registro");
    }
}
const res = document.getElementById('idbusqueda');
res.addEventListener('click', function () {
    event.preventDefault();
    cargarAjax();
});